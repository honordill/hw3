package Car;

import java.util.TreeMap;

public class Car {
    private int yearOfIssue = 1950;
    private String typeOfTheMotor = "GeForce GTX 1080 Ti";
    private int maxSpeedForNewCar = 300;
    private int timeToReachMaxSpeed = 5;
    private int maxPassengers = 4;
    private int currentPassengers;
    TreeMap<CarWheel,Double> wheels = new TreeMap();
    private int[] doors = {1, 2, 3, 4};
    private int currentMaxSpeed;
    private int currentSpeed = 0;

    public int getYearOfIssue() {
        return yearOfIssue;
    }

    public String getTypeOfTheMotor() {
        return typeOfTheMotor;
    }

    public int getMaxSpeedForNewCar() {
        return maxSpeedForNewCar;
    }

    public int getTimeToReachMaxSpeed() {
        return timeToReachMaxSpeed;
    }

    public int getMaxPassengers() {
        return maxPassengers;
    }

    public int getCurrentPassengers() {
        return currentPassengers;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public int changeCurrentSpeed(int currentSpeed) {
        return currentSpeed;
    }

    public int putOnePassenger() {
        currentPassengers = currentPassengers + 1;
        return currentPassengers;
    }

    public int dropOnePassenger() {
        currentPassengers = currentPassengers - 1;
        return currentPassengers;
    }

    public int dropAllPassengers() {
        currentPassengers = 0;
        return currentPassengers;
    }

    public int getDoorOnTheIndex(int j) {
        return doors[j];
    }

    public Object getWheelOnTheIndex(int i) {
        return wheels.get(i);
    }

    public TreeMap dropAllWheels() {
        wheels.clear();
        return wheels;
    }

    public int setWheels(int N) {
        for (int i = 0; i < N ; i++) {
            wheels.put(new CarWheel(),1.0);
        }
        return wheels.size() + N;
    }

    public double currentMaxSpeed() {

        if (currentPassengers <= 0) {
            return currentMaxSpeed = 0;
        } else {
            return maxSpeedForNewCar * wheels.get(0);

        }
    }

}
