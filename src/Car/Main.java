package Car;

public class Main {
    public static void main(String[] args) {

        Car car = new Car();

        CarWheel carWheel1 = new CarWheel();

        CarDoor cardoor = new CarDoor();


        System.out.println(car.getYearOfIssue());

        System.out.println(car.getTypeOfTheMotor());

        System.out.println(car.getMaxSpeedForNewCar());

        System.out.println(car.getTimeToReachMaxSpeed());

        System.out.println(car.getMaxPassengers());

        System.out.println(car.getCurrentPassengers());

        System.out.println(car.changeCurrentSpeed(100));

        System.out.println(car.getCurrentSpeed());

        System.out.println(car.putOnePassenger());

        System.out.println(car.dropOnePassenger());

        System.out.println(car.dropAllPassengers());

        System.out.println(car.setWheels(3));

        System.out.println(carWheel1.eraseWheelTo(0.5));

        System.out.print(carWheel1.getStatusOfTheWheel());

        System.out.println(car.dropAllWheels());

        System.out.println(cardoor.doorsClose());

        System.out.println(cardoor.doorsOpen());

        System.out.println(cardoor.doorsOpenClose());

        System.out.println(cardoor.getStatusD());

        System.out.println(cardoor.getStatusW());
        System.out.println(cardoor.windowsClose());
        System.out.println(cardoor.windowsOpen());
        System.out.println(cardoor.windowsOpenClose());

    }
}
