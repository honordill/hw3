package Car;

public class CarWheel {

    private double wheel;

    public CarWheel(double wheel){
        this.wheel = wheel;
    }

    public CarWheel(){
        this.wheel = 1.0;
    }

    double changeWheelToNew (){
        return wheel = 1.0;
    }

    double eraseWheelTo(double n){
        return wheel-n;
    }

    double getStatusOfTheWheel() {
       return wheel;
    }
}
